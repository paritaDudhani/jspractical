var assert = require('assert');

var voters = [
   {name:'Bob' , age: 30, voted: true},
   {name:'Jake' , age: 32, voted: true},
   {name:'Kate' , age: 25, voted: false},
   {name:'Sam' , age: 20, voted: false},
   {name:'Phil' , age: 21, voted: true},
   {name:'Ed' , age:55, voted:true},
   {name:'Tami' , age: 54, voted:true},
   {name: 'Mary', age: 31, voted: false},
   {name: 'Becky', age: 43, voted: false},
   {name: 'Joey', age: 41, voted: true},
   {name: 'Jeff', age: 30, voted: true},
   {name: 'Zack', age: 19, voted: false}
];

var group2 = [
  {name:'Bob' , age: 19, voted: true},
  {name:'Jake' , age: 12, voted: true},
  {name:'Kate' , age: 15, voted: false},
];

function voterResults(arr) {
  // your code here
  const obj = {
    youngVotes: 0,
    young:0,
    midVotes: 0,
    mid:0,
    oldVotes: 0,
    olds: 0
  }
  for(const itr of arr){
    if(itr.age>=18 && itr.age<=25){
      if(itr.voted==true)
        obj.youngVotes++;
      obj.young++;
    }
    else if(itr.age>=26 && itr.age<=35){
      if(itr.voted==true)
        obj.midVotes++
      obj.mid++;
    }
    else if(itr.age>=36 && itr.age<=55){
      if(itr.voted==true)
        obj.oldVotes++;
      obj.olds++;
    }
  }
  return obj;
}
console.log(voterResults(voters));
assert.notEqual(voterResults(voters),{ youngVotes: 1, young: 4, midVotes: 3, mid: 4, oldVotes: 3, olds: 4 });
assert.deepEqual(voterResults(group2),{ youngVotes: 1, young: 1, midVotes: 0, mid: 0, oldVotes: 0, olds: 0 });
// Returned value shown below:
/*
{ 
 youngVotes: 1,
 youth: 4,
 midVotes: 3,
 mids: 4,
 oldVotes: 3,
 olds: 4
}
*/
// Include how many of the potential voters were in the ages 18-25,
// how many from 26-35, how many from 36-55, and how many of each of those age ranges actually voted. 
// The resulting object containing this data should have 6 properties. 
// See the example output at the bottom.

